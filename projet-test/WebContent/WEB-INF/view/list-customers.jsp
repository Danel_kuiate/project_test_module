<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
	<title>Liste Usagers</title>
	
	<!-- reference our style sheet -->

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>TESTING CRUD APP</h2>
		</div>
	</div>
	
	
	<div id="container">
	
		<div id="content">
		
		<input type="button" value="Ajouter usager"
		      onclick="window.location.href='showFormForAdd'; return false;"
		      class="ass-button"
		/>
		
			<table>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Phone number</th>
					<th>Action</th>
				</tr>
				
				<!-- loop over and print our customers -->
				<c:forEach var="tempCustomer" items="${customers}">
				    
				    <!-- Upadate link -->
				    <c:url var="updateLink" value="/customer/showFormForUpdate">
				    <c:param name="customerId" value="${tempCustomer.id }" />
				    </c:url>
				    
				    
				    <!-- Delete Link -->
				     <c:url var="deleteLink" value="/customer/delete">
				    <c:param name="customerId" value="${tempCustomer.id }" />
				    </c:url>
				    
					<tr>
						<td> ${tempCustomer.firstName} </td>
						<td> ${tempCustomer.lastName} </td>
						<td> ${tempCustomer.email} </td>
						<td> ${tempCustomer.phoneNumber} </td>
						
						<td>
						<!-- Display the update and delete link -->
						<a href="${updateLink}">Update</a>
						|
						<a href="${deleteLink}"
						onclick="if(!(confirm('Voulez vous supprimer cet usager?')))return false">Delete</a>
						</td>	
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	

</body>

</html>

