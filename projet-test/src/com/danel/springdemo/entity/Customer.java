package com.danel.springdemo.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
    public static boolean isValidEmailAdress(String email) {
		boolean result = true;
		try {
			InternetAddress adresse = new InternetAddress(email);
			adresse.validate();
		}catch(AddressException ec) {
			result = false;
		}
		return result;
	}
	
	@Column(name="number")
	private String phoneNumber;
	
	 public static boolean isValidNumber(String s) 
	    { 
	        Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}"); 
	        Matcher m = p.matcher(s); 
	        return (m.find() && m.group().equals(s)); 
	    } 
	  
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		if (firstName== null) {
		      throw new IllegalArgumentException("can't be  null");
		    }
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		if (lastName == null) {
		      throw new IllegalArgumentException("Can't be null");
		    }
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		if (email== null) {
		      throw new IllegalArgumentException("Can't be null");
		    }
		this.email = email;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) {
		      throw new IllegalArgumentException("can't be null");
		    }
		this.phoneNumber = phoneNumber;
	}


	public Customer() {
		
	}
	
	


	public Customer(String firstName, String lastName, String email, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	
	


	public Customer(int id) {
		super();
		this.id = id;
	}


	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + "]";
	}
	
	
	
	
}
