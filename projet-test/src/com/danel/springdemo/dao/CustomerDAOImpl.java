package com.danel.springdemo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.danel.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
   
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public List<Customer> getCustomers() {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		//Creation of query
		Query<Customer> theQuery =
				currentSession.createQuery("from Customer order by lastName",Customer.class);
		
		//getting the result
		List<Customer> customers = theQuery.getResultList();
		return customers;
	}

	//Save Customer
	@Override
	public void  saveCustomer(Customer theCustomer) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		currentSession.saveOrUpdate(theCustomer);
	}
	

	@Override
	public Customer getCustomers(int theId) {
		//get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		//retrieve and read data from the db using pk
		Customer theCustomer = currentSession.get(Customer.class, theId);	
		return theCustomer;
	}

	@Override
	public void deleteCustomer(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query theQuery = 
				currentSession.createQuery("delete from Customer where id=:customerId");
		theQuery.setParameter("customerId",theId);
		
		//update delete
		
		theQuery.executeUpdate();
	}

}
