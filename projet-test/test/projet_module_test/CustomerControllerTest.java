package projet_module_test;


import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.hibernate.jdbc.Expectations;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.danel.springdemo.service.*;

import jakarta.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;

import com.danel.springdemo.controller.CustomerController;
import com.danel.springdemo.dao.CustomerDAOImpl;
import com.danel.springdemo.entity.Customer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerControllerTest {
	
	@Autowired
	public CustomerServiceImpl customerService;
	
	@MockBean
	private CustomerDAOImpl customerDAO;
	
	@Test
	public void getCustomersTest() {
		when(customerDAO.getCustomers()).thenReturn(Stream.of(new Customer("eric","erica","dd@gmail.com","0755958225"))).collect(Collectors.toList());
		assertEquals("eric",customerService.getCustomers());
		
	}
	
	@Test
	public void saveCustomerTest() {
		Customer customer = new Customer("eric","erica","dd@gmail.com","0755958225");
		when(customerDAO.saveCustomer(customer)).thenReturn(customer);
		}
	
	@Test
	public void deleteCustomerTest() {
		Customer customer = new Customer(1);
		customerService.deleteCustomer(1);
		verify(customerService,times(1)).delete(customer);
	}
	
	
	
	
	}

	

